import { Component, OnInit, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-child-template',
  templateUrl: './child-template.component.html',
  styleUrls: ['./child-template.component.scss']
})
export class ChildTemplateComponent implements OnInit {
  @Input() demoTemp: TemplateRef<any>;
  constructor() { }

  ngOnInit(): void {
  }

  funcDemo():void{
    console.log('xxxxxxxxxxx');
  }

}
