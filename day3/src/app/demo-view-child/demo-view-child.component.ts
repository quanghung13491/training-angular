import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChildTemplateComponent } from '../child-template/child-template.component';

@Component({
  selector: 'app-demo-view-child',
  templateUrl: './demo-view-child.component.html',
  styleUrls: ['./demo-view-child.component.scss']
})
export class DemoViewChildComponent implements OnInit {
@ViewChild('abc') child: ChildTemplateComponent;
@ViewChild('demoInput') input: ElementRef;
  constructor() { }

  ngOnInit(): void {
  }

  getChild():void {
    this.child.funcDemo();

    console.log(this.input.nativeElement.value);
  }

}
