import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  user = {
    name:"Đức Kiên",
    email: "kien@gmail.com",
    facebook: "fb.com/DucKien"
  }
  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(values){
    console.log(values);
  }
}
