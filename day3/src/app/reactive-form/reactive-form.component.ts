import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {
  fgUser: FormGroup;
  searchControl = new FormControl();
  constructor(private fb: FormBuilder) { }
  
  ngOnInit(): void {
    // this.fgUser = new FormGroup({
    //   name: new FormControl('',[Validators.required, Validators.minLength(5)]),
    //   social: new FormGroup({
    //     email: new FormControl,
    //     facebook: new FormControl
    //   })
    // })
    this.fgUser = this.fb.group({
      name: ['',[Validators.required, Validators.minLength(5)]],
      social: this.fb.group({
        email: '',
        facebook: ''
      })
    })
    // bắt sự kiện khi change value
    this.searchControl.valueChanges.subscribe(value=>{
      console.log(value);
    })

    // fake API, sử dụng patchValue để set lại giá trị

    setTimeout(()=>{
      this.fgUser.setValue({
        name: 'Hưng Nguyễn Quang',
        social:{
          email: 'quanghung@gmail.com'
        }
      });
      // Reset value
      setTimeout(()=>{
        this.fgUser.reset();
        }, 5000);
    }, 5000);
    

  }
  onSubmit(){
    console.log(this.fgUser);
    let nameControl = this.fgUser.get('name');
    console.log(nameControl);
  }
}
