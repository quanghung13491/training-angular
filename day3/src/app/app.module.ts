import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { TemplateComponent } from './template/template.component';
import { ChildTemplateComponent } from './child-template/child-template.component';
import { NgContentComponent } from './ng-content/ng-content.component';
import { NgContentChildComponent } from './ng-content/ng-content-child/ng-content-child.component';
import { FormComponent } from './form/form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { DemoViewChildComponent } from './demo-view-child/demo-view-child.component';

@NgModule({
  declarations: [
    AppComponent,
    TemplateComponent,
    ChildTemplateComponent,
    NgContentComponent,
    NgContentChildComponent,
    FormComponent,
    ReactiveFormComponent,
    DemoViewChildComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
