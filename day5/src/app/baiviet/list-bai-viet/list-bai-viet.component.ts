import { Component, OnInit } from '@angular/core';
import { BaivietService } from '../../shared/services/baiviet.service';
import { BaiViet } from '../models/baiviet.model';

@Component({
  selector: 'app-list-bai-viet',
  templateUrl: './list-bai-viet.component.html',
  styleUrls: ['./list-bai-viet.component.scss'],
})
export class ListBaiVietComponent implements OnInit {
  baiviets$: BaiViet[] = [];
  constructor(private api: BaivietService) { }

  ngOnInit(): void {
    this.getListBaiViet();
  }

  getListBaiViet(): void {
    this.baiviets$ = this.api.getListBaiViet();
  }
}
