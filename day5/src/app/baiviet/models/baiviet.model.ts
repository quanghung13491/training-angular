export class BaiViet {
    id: string;
    slug: string;
    title: string;
    content: string;
    updateAt: string;
}