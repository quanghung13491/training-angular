import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListBaiVietComponent } from './list-bai-viet/list-bai-viet.component';
import { ChiTietBaiVietComponent } from './chi-tiet-bai-viet/chi-tiet-bai-viet.component';


const routes: Routes = [
  {
    path: '',
    children:[
      {
        path:'',
        component: ListBaiVietComponent
      },
      {
        path:'chitietbaiviet/:slug',
        component: ChiTietBaiVietComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaivietRoutingModule { }
