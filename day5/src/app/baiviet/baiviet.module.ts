import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaivietRoutingModule } from './baiviet-routing.module';
import { ListBaiVietComponent } from './list-bai-viet/list-bai-viet.component';
import { ChiTietBaiVietComponent } from './chi-tiet-bai-viet/chi-tiet-bai-viet.component';
import { BaivietService } from '../shared/services/baiviet.service';

@NgModule({
  declarations: [ListBaiVietComponent, ChiTietBaiVietComponent],
  imports: [
    CommonModule,
    BaivietRoutingModule
  ],
  providers: [
    BaivietService
  ]
})
export class BaivietModule { }
