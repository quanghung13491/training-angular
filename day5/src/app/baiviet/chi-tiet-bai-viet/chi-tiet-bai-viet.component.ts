import { Component, OnInit } from '@angular/core';
import { BaiViet } from '../models/baiviet.model';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BaivietService } from '../../shared/services/baiviet.service';

@Component({
  selector: 'app-chi-tiet-bai-viet',
  templateUrl: './chi-tiet-bai-viet.component.html',
  styleUrls: ['./chi-tiet-bai-viet.component.scss']
})
export class ChiTietBaiVietComponent implements OnInit {
  baiviet$: Observable<BaiViet>;
  constructor(private route: ActivatedRoute, private api: BaivietService) { }

  ngOnInit(): void {
    let slug = this.route.snapshot.paramMap.get('slug');
    this.baiviet$ = this.api.getBaiVietBySlug(slug);
  }

}
