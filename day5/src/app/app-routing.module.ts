import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { CanAccessAdminGuardService } from './shared/services/Guard/can-access-admin-guard.service';
import { ConfirmDeactivateGuardService } from './shared/services/Guard/confirm-deactivate-guard.service';
import { CanLoadService } from './shared/services/Guard/can-load.service';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule),
    canActivate: [CanAccessAdminGuardService],
    canDeactivate: [ConfirmDeactivateGuardService],
    canLoad: [CanLoadService]
  },
  {
    path: '',
    loadChildren: () => import('./baiviet/baiviet.module').then((m) => m.BaivietModule)
  },
  {
    path:'login',
    component: LoginComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
