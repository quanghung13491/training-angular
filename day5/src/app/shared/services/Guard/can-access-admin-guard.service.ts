import { Injectable } from '@angular/core';
import { CustomStorage } from '../../CustomStorage';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CanAccessAdminGuardService implements CanActivate {

  constructor(private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    // Nếu là admin
    if(CustomStorage.currentUser && CustomStorage.currentUser.role === 'admin')
      return true;
    else
      this.router.navigateByUrl('/login');
  }
}
