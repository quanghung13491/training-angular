import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CheckDeactivate } from '../../check-deactivate';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDeactivateGuardService implements CanDeactivate<CheckDeactivate> {

  constructor() { }
  canDeactivate(component: CheckDeactivate, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    return window.confirm('Bạn có muốn rời trang này?');
  }
}
