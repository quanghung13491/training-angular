import { Injectable } from '@angular/core';
import { CanLoad, Route } from '@angular/router';
import { CustomStorage } from '../../CustomStorage';

@Injectable({
  providedIn: 'root'
})
export class CanLoadService implements CanLoad {

  constructor() { }
  canLoad(route: Route, segments: import("@angular/router").UrlSegment[]): boolean | import("rxjs").Observable<boolean> | Promise<boolean> {
    // if(CustomStorage.currentUser && CustomStorage.currentUser.role === 'admin')
    //   return true;
    // else
    //   return false;
    return true;
  }
}
