import { Injectable } from '@angular/core';
import { BaiViet } from '../../baiviet/models/baiviet.model';
import { of, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BaivietService {
  listbaiViet: BaiViet[] = [
    {
      id: "1",
      slug: "bai-viet-1",
      title: "Bai viet 1",
      content: "noi dung bai viet 1",
      updateAt: "2020-07-19T13:26:31.785Z",
    },
    {
      id: "2",
      slug: "bai-viet-2",
      title: "Bai viet 2",
      content: "Noi dung bai viet 2",
      updateAt: "2020-07-20:00:00.000Z",
    },
  ];
  constructor() { }

  getListBaiViet(): BaiViet[]{
    return this.listbaiViet;
  }

  getBaiVietBySlug(slug: String): Observable<BaiViet> {
    let article = this.listbaiViet.find(x => x.slug === slug)
    return of(article).pipe(delay(500));
  }

}
