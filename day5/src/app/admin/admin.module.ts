import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { QuanlybaivietComponent } from './quanlybaiviet/quanlybaiviet.component';
import { AdminComponent } from './admin.component';


@NgModule({
  declarations: [QuanlybaivietComponent, AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
