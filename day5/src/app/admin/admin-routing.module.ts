import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { QuanlybaivietComponent } from './quanlybaiviet/quanlybaiviet.component';


const routes: Routes = [
  {
    path:'',
    component: AdminComponent,
    children: [
      {
        path: '',
        component: QuanlybaivietComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
