import { Component, OnInit } from '@angular/core';
import { BaiViet } from 'src/app/baiviet/models/baiviet.model';
import { BaivietService } from 'src/app/shared/services/baiviet.service';

@Component({
  selector: 'app-quanlybaiviet',
  templateUrl: './quanlybaiviet.component.html',
  styleUrls: ['./quanlybaiviet.component.scss']
})
export class QuanlybaivietComponent implements OnInit {
  baiviets$: BaiViet[] = [];
  constructor(private api: BaivietService) { }

  ngOnInit(): void {
    this.getListBaiViet();
  }

  getListBaiViet(): void {
    this.baiviets$ = this.api.getListBaiViet();
  }

}
