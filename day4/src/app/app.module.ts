import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookService } from './services/book.service';
import { ListBookComponent } from './components/list-book/list-book.component';
import { HttpClientModule } from '@angular/common/http';
import { ListPostComponent } from './components/list-post/list-post.component';
import { PostService } from './services/post.service';
import { RxjsComponent } from './components/rxjs/rxjs.component';
import { RxjsOperatorsComponent } from './components/rxjs-operators/rxjs-operators.component';
import { ComponentAComponent } from './components/componentA/componentA.component';
import { componentBComponent } from './components/ComponentB/componentB.component';
import { MessageService } from "./services/message.service";

@NgModule({
  declarations: [
    AppComponent,
    ListBookComponent,
    ListPostComponent,
    RxjsComponent,
    RxjsOperatorsComponent,
    ComponentAComponent,
    componentBComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [BookService, PostService, PostService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
