import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpEvent } from '@angular/common/http';
import { IPost } from '../models/post.model';


@Injectable({
  providedIn: 'root'
})
export class Post2Service {

  constructor(private httpClient: HttpClient) { }

  // getListPosts(): Promise<IPost[]> {
  //   return this.httpClient.get<IPost[]>('https://jsonplaceholder.typicode.com/posts',
  //                                       {headers:{token: 'xxxxxx'}}).toPromise();
  // }

  // getListPosts(): Promise<IPost[]> {
  //   return this.httpClient.get<IPost[]>('https://jsonplaceholder.typicode.com/posts',
  //                                       { headers: new HttpHeaders({ token: 'xxxxx' })}).toPromise();
  // }

  // getListPosts(): Promise<IPost[]> {
  //   let headers: HttpHeaders = new HttpHeaders();
  //   headers = headers.set('token', 'xxxxx');
  //   return this.httpClient.get<IPost[]>('https://jsonplaceholder.typicode.com/posts',
  //     { headers}).toPromise();
  // }

  // getListPosts(): Promise<HttpResponse<IPost[]>> {
  //   return this.httpClient.get<IPost[]>('https://jsonplaceholder.typicode.com/posts',
  //     { observe: 'response' }).toPromise();
  // }

  getListPosts(): Promise<HttpEvent<IPost[]>> {
    return this.httpClient.get<IPost[]>('https://jsonplaceholder.typicode.com/posts',
      { observe: 'events' }).toPromise();
  }

  createPost(post: IPost): Promise<IPost> {
    return this.httpClient.post<IPost>('https://jsonplaceholder.typicode.com/posts/create', post).toPromise();
  }

  updatePost(postId: number, post: IPost): Promise<IPost> {
    return this.httpClient.put<IPost>(`https://jsonplaceholder.typicode.com/posts/${postId}`, post).toPromise();
  }

  deletePost(postId: number): Promise<any> {
    return this.httpClient.delete(`https://jsonplaceholder.typicode.com/posts/${postId}`).toPromise();
  }
}
