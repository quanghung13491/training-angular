import { Injectable } from '@angular/core';
import { IBook } from '../models/book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  title: String = "Danh sách book";
  constructor() { }

  fetchData():IBook[]{
    let listBook : IBook[] = [
      {
        id: 1,
        name: 'Book A',
        author: 'Nguyễn A',
        price: 30000
      },
      {
        id: 2,
        name: 'Book B',
        author: 'Nguyễn B',
        price: 50000
      },
      {
        id: 3,
        name: 'Book C',
        author: 'Nguyễn C',
        price: 70000
      }
    ];
    return listBook;
  }
}
