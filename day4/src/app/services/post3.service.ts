import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';
import { BaseResponse } from '../models/baseResponse.model';
import { IPost } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class Post3Service extends BaseHttpService {

  constructor(protected http: HttpClient) { 
    super(http);
  }
  async getListPosts(model:any): Promise<BaseResponse<any>> {
    return await super.getAsync('posts',model);
  }

  async createPost(post: IPost): Promise<BaseResponse<any>> {
    return await super.postAsync('posts/create', post);
  }

  async updatePost(post: IPost): Promise<BaseResponse<any>> {
    return await super.putAsync('posts/update', post);
  }

  async deletePost(postId: number): Promise<BaseResponse<any>> {
    return super.deleteAsync('posts/delete', {id: postId});
  }
}
