export class BaseResponse<T> {
    status: number;
    message: string;
    data: T;
    constructor(params){
        this.status  = params.status;
        this.message = params.message;
        this.data = params.data;
    }
}