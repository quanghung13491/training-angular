import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.scss']
})
export class RxjsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    // Tạo 1 Observable
    const observable = new Observable(function subscribe(observer) {
      const id = setTimeout(() => {
        observer.next('Hello Rxjs');
        observer.complete();
      }, 1000);
      return function unsubscribe() {
        clearTimeout(id);
      }
    });
    //Invoke Observable
    const subscription = observable.subscribe({
      next: (value) => {
        console.log(value);
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        console.log('Done');
      }
    });

    // Sau 5s hủy việc thực thi
    setTimeout(() => {
      subscription.unsubscribe();
    }, 5000);


    //observer
    const observer = {
      next: x => console.log('Observer got a next value: ' + x),
      error: err => console.error('Observer got an error: ' + err),
      complete: () => console.log('Observer got a complete notification'),
    };

    observable.subscribe(observer);

    observable.subscribe(
      x => console.log('Observer got a next value: ' + x),
      null,
      () => console.log('Observer got a complete notification')
    );

  }

}
