import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-componentA',
  templateUrl: './componentA.component.html',
  styleUrls: ['./componentA.component.scss']
})
export class ComponentAComponent implements OnInit {

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
  }

  sendMessage(): void {
    // send message to subscribers via observable subject
    this.messageService.sendMessage('Message from Component A to component B!');
  }

  clearMessage(): void {
    // clear message
    this.messageService.clearMessage();
  }

}
