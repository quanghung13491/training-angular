import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-componentB',
  templateUrl: './componentB.component.html',
  styleUrls: ['./componentB.component.scss']
})
export class componentBComponent implements OnInit {
  message: any;
  subscription: Subscription;
  constructor(private messageService: MessageService) { 
  }

  ngOnInit(): void {
    this.subscription = this.messageService.getMessage().subscribe(message => this.message = message?.text);
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
}

}
