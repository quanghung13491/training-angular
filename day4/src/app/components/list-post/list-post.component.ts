import { Component, OnInit } from '@angular/core';
import { Post2Service } from 'src/app/services/post2.service';
import { PostService } from 'src/app/services/post.service';
import { Post3Service } from 'src/app/services/post3.service';
import { IPost } from 'src/app/models/post.model';

@Component({
  selector: 'app-list-post',
  templateUrl: './list-post.component.html',
  styleUrls: ['./list-post.component.scss']
})
export class ListPostComponent implements OnInit {

  listPost: IPost[] = [];

  constructor(
    private postService: PostService, 
    private post2Service: Post2Service,
    private post3Service: Post3Service
    ) { }

  ngOnInit(): void {
    this.getListPost();
  }

  getListPost():void {
    this.postService.getListPosts().subscribe(
      (posts: IPost[]) => this.listPost = posts
    )
  }

    async getListPost2():Promise<void> {
     console.log(await this.post2Service.getListPosts());
  }


  async getListPost3():Promise<void> {
    let result = await this.post3Service.getListPosts(null);
  }

}
