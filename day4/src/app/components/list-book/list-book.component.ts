import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { IBook } from 'src/app/models/book.model';

@Component({
  selector: 'app-list-book',
  templateUrl: './list-book.component.html',
  styleUrls: ['./list-book.component.scss'],
})
export class ListBookComponent implements OnInit {
  listBook: IBook[] = [];
  title: String;
  constructor(private bookService: BookService) { }

  ngOnInit(): void {
    this.title = this.bookService.title;
    this.getListBook();
  }

  getListBook():void{
    setTimeout(() => {
      this.listBook = this.bookService.fetchData();
    }, 5000);
  }
}
