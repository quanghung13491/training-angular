import { Component, OnInit } from '@angular/core';
import { of, from, fromEvent, interval, timer, throwError } from 'rxjs';

@Component({
  selector: 'app-rxjs-operators',
  templateUrl: './rxjs-operators.component.html',
  styleUrls: ['./rxjs-operators.component.scss']
})
export class RxjsOperatorsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    // observer dùng chung
    const observer = {
      next: (val) => console.log(val),
      error: (err) => console.log(err),
      complete: () => console.log('complete'),
    };

    // ----------Of()------------
    of('hello').subscribe(observer);
    of([1, 2, 3]).subscribe(observer);
    of(1, 2, 3, 'hello', 'world', { foo: 'bar' }, [4, 5, 6]).subscribe(observer);

    // --------from()------------
    // array
    from([1, 2, 3]).subscribe(observer);
    // string
    from('hello world').subscribe(observer);

    // map
    const map = new Map();
    map.set(1, 'hello');
    map.set(2, 'bye');
    from(map).subscribe(observer);

    //set
    const set = new Set();
    set.add(1);
    set.add(2);
    from(set).subscribe(observer);

    // Promise
    from(Promise.resolve('hello world')).subscribe(observer);


    // -----------fromEvent()-------------
    const btn = document.querySelector('#btn');
    const input = document.querySelector('#input');

    fromEvent(btn, 'click').subscribe(observer);

    fromEvent(input, 'keydown').subscribe(observer);

    // ----------- interval()----------

    interval(1000).subscribe(observer);

    // -----------timer()------------
    //Cách 1
    timer(1000).subscribe(observer);
    // Cách 2
    timer(1000, 1000).subscribe(observer);

    // throwError
    throwError('an error').subscribe(observer);

  }

}
